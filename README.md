# DNN Speech Recognizer #

### Introduction ###

In this project, We develop an ASR pipeline using LibriSpeech dataset and Spectrogram feature. The neural network model consists of CNN 1D, dropout layer, and Bidirectional RNN. The [TimeDistributed](https://keras.io/layers/wrappers/) layer is added to find more complex patterns in the dataset. The unrolled snapshot of the architecture is depicted below.

![Architecture](images/architecture.png)

For this project, we won't use the raw audio waveform as input to your model.  Instead, we first performs a pre-processing step to convert the raw audio to a feature representation that has historically proven successful for ASR models.  Our acoustic model will accept the feature representation as input.

We chosse to use an audio feature representation, i.e the [spectrogram](https://www.youtube.com/watch?v=_FatxGN3vAM). The soruce code for calculating the spectrogram was borrowed from [this repository](https://github.com/baidu-research/ba-dls-deepspeech).  The code returns the spectrogram as a 2D tensor, where the first (_vertical_) dimension indexes time, and the second (_horizontal_) dimension indexes frequency. To speed the convergence of your algorithm, we also normalized the spectrogram. 

### Dataset
[LibriSpeech](http://www.danielpovey.com/files/2015_icassp_librispeech.pdf) is a large corpus of English-read speech, designed for training and evaluating models for ASR.  The dataset contains 1000 hours of speech derived from audiobooks.  We will work with a small subset in this project, since larger-scale data would take a long while to train.

### Results ###

- Training loss: 85.8435 
- Validation loss: 111.5457